package com.realjamapps.yagallery.data

import android.arch.persistence.room.*
import com.realjamapps.yagallery.storage.converters.UUIDConverter
import java.util.*


@Entity(tableName = "CORGI",
        indices = [(Index(value = ["UUID"], unique = true))])
data class CorgiData(

        @ColumnInfo(name = "UUID")
        @PrimaryKey
        @field:TypeConverters(UUIDConverter::class) val uuid: UUID,

        @ColumnInfo(name = "PHOTO_URL") val imageUrl: String
)
