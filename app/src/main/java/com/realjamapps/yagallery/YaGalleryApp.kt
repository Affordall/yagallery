package com.realjamapps.yagallery

import android.app.Application
import android.os.StrictMode
import android.provider.Settings
import android.support.v7.app.AppCompatDelegate
import com.crashlytics.android.Crashlytics
import com.crashlytics.android.core.CrashlyticsCore
import com.realjamapps.yagallery.di.components.ApplicationComponent
import com.realjamapps.yagallery.di.components.DaggerApplicationComponent
import com.realjamapps.yagallery.di.modules.ApplicationModule
import com.realjamapps.yagallery.di.modules.DataModule
import com.realjamapps.yagallery.di.modules.StorageModule
import io.fabric.sdk.android.Fabric


class YaGalleryApp : Application() {

    private lateinit var applicationComponent: ApplicationComponent

    override fun onCreate() {
        super.onCreate()
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)

        buildGraphAndInject()
        setupCrashReports()

        if (BuildConfig.DEBUG) {
            strictMode()
        }
    }

    private fun buildGraphAndInject() {
        applicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(ApplicationModule(this))
                .dataModule(DataModule())
                .storageModule(StorageModule(this))
                .build()
    }

    private fun setupCrashReports() {
        Fabric.with(this,
                Crashlytics.Builder()
                        .core(
                                CrashlyticsCore.Builder()
                                        .disabled(BuildConfig.DEBUG)
                                        .build()
                        )
                        .build()
        )
        Crashlytics.setUserIdentifier(androidID)
    }

    fun getApplicationComponent(): ApplicationComponent {
        return applicationComponent
    }

    private val androidID: String
        get() = android.provider.Settings.System.getString(applicationContext.contentResolver, Settings.Secure.ANDROID_ID)

    private fun strictMode() {
        StrictMode.setThreadPolicy(StrictMode.ThreadPolicy.Builder()
                .detectAll()
                .penaltyLog()
                .penaltyDialog()
                .build())
        StrictMode.setVmPolicy(StrictMode.VmPolicy.Builder().detectAll()
                .penaltyLog()
                .build())
    }
}
