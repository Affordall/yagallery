package com.realjamapps.yagallery.dto

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class CorgiResponseDto(
        @SerializedName("status")
        @Expose
        val status: String,
        @SerializedName("message")
        @Expose
        val message: List<String>?
)