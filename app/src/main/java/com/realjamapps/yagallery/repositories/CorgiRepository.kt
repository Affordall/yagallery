package com.realjamapps.yagallery.repositories

import com.realjamapps.yagallery.models.Corgi
import io.reactivex.Single
import java.util.*


interface CorgiRepository {
    fun findByUuid(uuid: UUID): Single<Corgi?>
    fun insertEntry(corgi: Corgi): Single<Boolean>
    fun insertEntryList(corgiList: List<Corgi>): Single<Boolean>
    fun loadList(): Single<List<Corgi>>
}