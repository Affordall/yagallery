package com.realjamapps.yagallery.repositories

import com.realjamapps.yagallery.CorgiMapper
import com.realjamapps.yagallery.data.CorgiData
import com.realjamapps.yagallery.models.Corgi
import com.realjamapps.yagallery.storage.CorgiDataStorage
import io.reactivex.Single
import java.util.*
import javax.inject.Inject
import javax.inject.Singleton


@Singleton
class CorgiDataRepository @Inject constructor(
        private val dataStorage: CorgiDataStorage
) : CorgiRepository {

    override fun findByUuid(uuid: UUID): Single<Corgi?> {
        return Single.fromCallable {
            val authorData: CorgiData? = dataStorage.findById(uuid)
            return@fromCallable authorData?.let { data ->
                return@let CorgiMapper.fromDataToEntity(data)
            }
        }
    }

    override fun insertEntry(corgi: Corgi): Single<Boolean> {
        return Single.fromCallable {
            val corgiData = CorgiMapper.fromEntityToData(corgi)
            dataStorage.insert(corgiData)
            return@fromCallable true
        }
    }

    override fun insertEntryList(corgiList: List<Corgi>): Single<Boolean> {
        return Single.fromCallable {
            val corgiDataList: MutableList<CorgiData> = ArrayList()
            corgiList.forEach { corgi ->
                val corgiData = CorgiMapper.fromEntityToData(corgi)
                corgiDataList.add(corgiData)
            }
            dataStorage.insertAll(corgiDataList)
            return@fromCallable true
        }
    }

    override fun loadList(): Single<List<Corgi>> {
        return Single.fromCallable {
            val resultList: MutableList<Corgi> = ArrayList()
            val list: List<CorgiData>? = dataStorage.loadAll()
            list?.forEach { data ->
                resultList.add(
                        CorgiMapper.fromDataToEntity(data)
                )
            }
            return@fromCallable resultList
        }
    }
}