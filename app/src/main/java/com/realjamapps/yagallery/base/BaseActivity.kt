package com.realjamapps.yagallery.base

import android.os.Bundle
import android.support.v7.app.AppCompatActivity

import com.realjamapps.yagallery.YaGalleryApp
import com.realjamapps.yagallery.di.modules.ActivityModule


abstract class BaseActivity : AppCompatActivity() {

    protected val applicationComponent
        get() = (application as YaGalleryApp).getApplicationComponent()


    protected val activityModule: ActivityModule
        get() = ActivityModule(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.applicationComponent.inject(this)
    }
}
