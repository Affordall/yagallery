package com.realjamapps.yagallery.base

import android.os.Bundle
import android.support.annotation.LayoutRes
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.realjamapps.yagallery.di.IHasComponent


abstract class BaseFragment : Fragment() {

    private var rootView: View? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        rootView = inflater.inflate(getLayoutResource(), container, false) ?: null
        return rootView
    }

    protected fun <C> getComponent(componentType: Class<C>): C {
        return componentType.cast((activity as IHasComponent<C>).getComponent())
    }

    protected abstract fun getLayoutResource(): Int
}