package com.realjamapps.yagallery.interactors

import android.os.ParcelUuid
import com.realjamapps.yagallery.api.IApiService
import com.realjamapps.yagallery.dto.CorgiResponseDto
import com.realjamapps.yagallery.logger.LogUtils
import com.realjamapps.yagallery.models.Corgi
import com.realjamapps.yagallery.repositories.CorgiRepository
import io.reactivex.Single
import retrofit2.Call
import retrofit2.Response
import java.io.IOException
import java.io.InterruptedIOException
import java.util.*
import java.util.concurrent.Callable
import javax.inject.Inject


class CorgiUseCase @Inject constructor(
        private val corgiRepository: CorgiRepository,
        val apiService: IApiService
) {

    private lateinit var call: Call<CorgiResponseDto>

    fun loadListFromStorage(): Single<List<Corgi>> {
        return corgiRepository.loadList()
    }

    @Throws(IllegalStateException::class)
    fun downloadListAndSaveToStorage(): Single<Boolean> {
        return Single.fromCallable { Callable<List<String>> { getCorgiList() }.call()
        }.doOnError {
            throw it
        }
                .flatMap { corgiList ->
            saveListToStorage(corgiList)
        }
    }

    private fun saveListToStorage(urlList: List<String>): Single<Boolean> {
        return corgiRepository.insertEntryList(
                urlList.mapTo(arrayListOf(), { imageUrl ->
                    Corgi(
                            parcelUuid = ParcelUuid(UUID.randomUUID()),
                            imageUrl = imageUrl
                    )
                }))
    }

    @Throws(IllegalStateException::class)
    private fun getCorgiList(): List<String> {
        val response: Response<CorgiResponseDto>? = getCorgiListResponse()
        return response?.body()?.message ?: throw IllegalStateException("Reached timeout")
    }

    private fun getCorgiListResponse(): Response<CorgiResponseDto>? {
        call = apiService.getCorgiList()
        return try {
            if (!call.isExecuted) {
                call.execute()
            } else {
                null
            }
        } catch (e: IOException) {
            LogUtils.logAndReportError(CorgiUseCase::class.java, e)
            null
        }
    }
}