package com.realjamapps.yagallery

import android.os.ParcelUuid
import com.realjamapps.yagallery.data.CorgiData
import com.realjamapps.yagallery.models.Corgi


object CorgiMapper {

    fun fromDataToEntity(corgiData: CorgiData): Corgi {
        return Corgi(
                parcelUuid = ParcelUuid(corgiData.uuid),
                imageUrl = corgiData.imageUrl
        )
    }

    fun fromEntityToData(corgi: Corgi): CorgiData {
        return CorgiData(
                uuid = corgi.parcelUuid.uuid,
                imageUrl = corgi.imageUrl
        )
    }
}