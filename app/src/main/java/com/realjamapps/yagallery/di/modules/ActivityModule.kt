package com.realjamapps.yagallery.di.modules


import android.support.v7.app.AppCompatActivity
import com.realjamapps.yagallery.di.ActivityScope

import dagger.Module
import dagger.Provides

@Module
class ActivityModule(private val activity: AppCompatActivity) {

    @Provides
    @ActivityScope
    fun provideActivity(): AppCompatActivity {
        return this.activity
    }
}
