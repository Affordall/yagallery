package com.realjamapps.yagallery.di.modules

import com.realjamapps.yagallery.repositories.CorgiDataRepository
import com.realjamapps.yagallery.repositories.CorgiRepository
import dagger.Binds
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module(includes = [
    NetworkModule::class,
    StorageModule::class
])
class DataModule {

    @Singleton
    @Provides
    fun provideCorgiRepository(corgiDataRepository: CorgiDataRepository): CorgiRepository {
        return corgiDataRepository
    }
}