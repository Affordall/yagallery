package com.realjamapps.yagallery.di

interface IHasComponent<C> {
    fun getComponent(): C
}
