package com.realjamapps.yagallery.di.modules

import com.google.gson.GsonBuilder
import com.realjamapps.yagallery.BuildConfig
import com.realjamapps.yagallery.api.IApiService
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Converter
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton


@Module()
class NetworkModule {

    @Provides
    @Singleton
    fun provideOkHttpClient(): OkHttpClient {
        val builder = OkHttpClient.Builder()

        builder.interceptors().clear()
        interceptLogging(builder)

        builder.connectTimeout(0, TimeUnit.HOURS)
        builder.readTimeout(0, TimeUnit.HOURS)
        builder.writeTimeout(0, TimeUnit.HOURS)

        return builder.build()
    }

    private fun interceptLogging(builder: OkHttpClient.Builder) {
        if (BuildConfig.DEBUG) {
            val httpLoggingInterceptor = HttpLoggingInterceptor()
            httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
            builder.interceptors().add(httpLoggingInterceptor)
        }
    }

    @Provides
    @Singleton
    fun provideBaseUrl(): String = BuildConfig.API_URL

    @Provides
    @Singleton
    fun provideConverter(): Converter.Factory = GsonConverterFactory.create(GsonBuilder().create())

    @Provides
    @Singleton
    fun provideRetrofit(httpClient: OkHttpClient, baseUrl: String, converter: Converter.Factory): Retrofit {
        return Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(converter)
                .client(httpClient).build()

    }

    @Provides
    @Singleton
    fun provideService(retrofit: Retrofit): IApiService {
        return retrofit.create(IApiService::class.java)
    }
}
