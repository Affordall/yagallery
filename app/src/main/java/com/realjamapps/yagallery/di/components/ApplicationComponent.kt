package com.realjamapps.yagallery.di.components

import android.content.Context
import com.realjamapps.yagallery.api.IApiService
import com.realjamapps.yagallery.base.BaseActivity
import com.realjamapps.yagallery.di.modules.ApplicationModule
import com.realjamapps.yagallery.di.modules.DataModule
import com.realjamapps.yagallery.repositories.CorgiRepository
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = arrayOf(ApplicationModule::class, DataModule::class))
interface ApplicationComponent {

    fun inject(baseActivity: BaseActivity)

    fun context(): Context

    fun corgiRepository(): CorgiRepository

    fun apiService(): IApiService
}
