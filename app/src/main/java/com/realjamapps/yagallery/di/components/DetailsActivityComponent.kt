package com.realjamapps.yagallery.di.components

import com.realjamapps.yagallery.di.ActivityScope
import com.realjamapps.yagallery.di.modules.ActivityModule
import com.realjamapps.yagallery.views.impl.DetailActivity
import com.realjamapps.yagallery.views.impl.MainFragment
import dagger.Component


@ActivityScope
@Component(dependencies = [ApplicationComponent::class], modules = [ActivityModule::class])
interface DetailsActivityComponent : ActivityComponent {
    fun inject(detailActivity: DetailActivity)
}