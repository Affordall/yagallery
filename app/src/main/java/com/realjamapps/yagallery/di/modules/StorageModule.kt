package com.realjamapps.yagallery.di.modules

import android.app.Application
import android.arch.persistence.room.Room
import com.realjamapps.StorageTransactionManager
import com.realjamapps.yagallery.Database
import com.realjamapps.yagallery.storage.CorgiDataStorage
import dagger.Module
import dagger.Provides
import java.util.concurrent.locks.ReadWriteLock
import java.util.concurrent.locks.ReentrantReadWriteLock
import javax.inject.Singleton


@Module
class StorageModule(application: Application) {

    private val DATABASE_FILE_NAME: String = "gallery-db"
    private val database: Database = Room.databaseBuilder(application, Database::class.java, DATABASE_FILE_NAME)
            .fallbackToDestructiveMigration()
            .build()

    @Singleton
    @Provides
    fun provideStorageTransactionManager(databaseLock: ReadWriteLock): StorageTransactionManager {
        return StorageTransactionManager(database, databaseLock)
    }

    @Singleton
    @Provides
    fun provideDatabaseLock(): ReadWriteLock {
        return ReentrantReadWriteLock()
    }

    @Singleton
    @Provides
    fun provideCorgiDataStorage(databaseLock: ReadWriteLock, storageTransactionManager: StorageTransactionManager): CorgiDataStorage {
        return CorgiDataStorage(databaseLock, database.corgiDataDao(), storageTransactionManager)
    }
}