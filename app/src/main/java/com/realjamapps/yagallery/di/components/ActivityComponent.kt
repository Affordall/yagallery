package com.realjamapps.yagallery.di.components


import android.support.v7.app.AppCompatActivity
import com.realjamapps.yagallery.di.ActivityScope
import com.realjamapps.yagallery.di.modules.ActivityModule

import dagger.Component

@ActivityScope
@Component(modules = arrayOf(ActivityModule::class))
interface ActivityComponent {

    fun activity(): AppCompatActivity

}
