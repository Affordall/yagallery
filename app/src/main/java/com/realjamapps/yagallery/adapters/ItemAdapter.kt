package com.realjamapps.yagallery.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.realjamapps.yagallery.R
import com.realjamapps.yagallery.adapters.viewholder.ItemViewHolder
import com.realjamapps.yagallery.models.Corgi
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject

class ItemAdapter(
        val data: MutableList<Corgi>? = ArrayList()
) : RecyclerView.Adapter<RecyclerView.ViewHolder>(), IUpdateDataAdapter {

    private val clickSubject = PublishSubject.create<Corgi>()
    val onClickObservable: Observable<Corgi> = clickSubject

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder? {
        val viewHolder: RecyclerView.ViewHolder
        val inflater = LayoutInflater.from(parent.context)
        val item = inflater.inflate(R.layout.viewholder_layout, parent, false)
        viewHolder = ItemViewHolder(item)
        viewHolder.itemView.setOnClickListener {
            val position = viewHolder.layoutPosition
            if (position != RecyclerView.NO_POSITION) {
                val corgi = data?.get(position)
                corgi?.let {
                    clickSubject.onNext(it)
                }
            }
        }
        return viewHolder
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val corgi = data?.get(position)
        corgi?.let {
            (holder as ItemViewHolder).bind(it)
        }
    }

    override fun getItemCount(): Int {
        return data?.size ?: 0
    }

    override fun updateData(list: List<Corgi>) {
        data?.clear()
        data?.addAll(list)
        notifyDataSetChanged()
    }
}

