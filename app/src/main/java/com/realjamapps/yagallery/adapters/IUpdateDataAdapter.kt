package com.realjamapps.yagallery.adapters

import com.realjamapps.yagallery.models.Corgi

interface IUpdateDataAdapter {
    fun updateData(list: List<Corgi>)
}