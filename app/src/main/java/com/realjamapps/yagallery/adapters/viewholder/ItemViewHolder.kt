package com.realjamapps.yagallery.adapters.viewholder

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.realjamapps.yagallery.R
import com.realjamapps.yagallery.models.Corgi


class ItemViewHolder constructor(view: View) : RecyclerView.ViewHolder(view) {

    private val imageThumbnail: ImageView by lazy {
        view.findViewById<ImageView>(R.id.viewThumbnail) as ImageView
    }

    init {
        itemView.isClickable = true
    }

    fun bind(corgi: Corgi) = with(itemView) {
        imageThumbnail.loadUrl(corgi.imageUrl)
    }

    private fun ImageView.loadUrl(url: String?) {
        Glide.with(context)
                .load(url)
                .apply(RequestOptions()
                        .centerCrop()
                        .placeholder(android.R.drawable.stat_sys_download_done)
                )
                .into(this)
    }

}
