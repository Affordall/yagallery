package com.realjamapps.yagallery.models

import android.os.Parcel
import android.os.ParcelUuid
import android.os.Parcelable


data class Corgi(val parcelUuid: ParcelUuid, val imageUrl: String) : Parcelable {

    constructor(parcel: Parcel) : this(
            parcel.readParcelable(ParcelUuid::class.java.classLoader),
            parcel.readString())

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeParcelable(parcelUuid, flags)
        parcel.writeString(imageUrl)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Corgi> {
        override fun createFromParcel(parcel: Parcel): Corgi {
            return Corgi(parcel)
        }

        override fun newArray(size: Int): Array<Corgi?> {
            return arrayOfNulls(size)
        }
    }
}
