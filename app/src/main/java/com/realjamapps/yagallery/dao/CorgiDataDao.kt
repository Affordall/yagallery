package com.realjamapps.yagallery.dao

import android.arch.persistence.room.*
import com.realjamapps.yagallery.data.CorgiData
import com.realjamapps.yagallery.storage.converters.UUIDConverter
import java.util.*


@Dao
@TypeConverters(UUIDConverter::class)
internal interface CorgiDataDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(corgiData: CorgiData)

    @Query("SELECT * FROM CORGI WHERE UUID = :uuid LIMIT 1")
    fun findById(uuid: UUID): CorgiData?

    @Query("SELECT * FROM CORGI")
    fun loadAll(): List<CorgiData>?
}