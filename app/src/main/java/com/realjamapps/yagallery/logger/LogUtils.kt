package com.realjamapps.yagallery.logger

import android.util.Log
import com.crashlytics.android.Crashlytics
import com.realjamapps.yagallery.BuildConfig
import io.fabric.sdk.android.Fabric


object LogUtils {

    fun log(clazz: Class<*>, message: String) {
        if (BuildConfig.DEBUG) {
            Log.w(clazz.simpleName, message)
        }
    }

    fun log(tag: String, message: String) {
        if (BuildConfig.DEBUG) {
            Log.w(tag, message)
        }
    }

    fun log(priority: Int, tag: String, message: String) {
        if (BuildConfig.DEBUG) {
            when (priority) {
                Log.VERBOSE -> Log.v(tag, message)
                Log.DEBUG -> Log.d(tag, message)
                Log.INFO -> Log.i(tag, message)
                Log.WARN -> Log.w(tag, message)
                Log.ERROR -> Log.e(tag, message)
            }
        }
    }

    fun logAndReportError(clazz: Class<*>, t: Throwable) {
        Log.e(clazz.simpleName, "Error message: " + t.message, t)
        sendReport(t)
    }

    fun logAndReportError(tag: String, t: Throwable) {
        Log.e(tag, "Error message: " + t.message, t)
        sendReport(t)
    }

    private fun sendReport(throwable: Throwable) {
        // Check Fabric is initialized
        // If no (is TEST case) -> no logging crashes.
        if (Fabric.isInitialized()) {
            Crashlytics.logException(throwable)
        }
    }
}
