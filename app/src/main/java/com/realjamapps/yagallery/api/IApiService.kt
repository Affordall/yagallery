package com.realjamapps.yagallery.api

import com.realjamapps.yagallery.dto.CorgiResponseDto
import retrofit2.Call
import retrofit2.http.GET


interface IApiService {

    @GET("api/breed/corgi/cardigan/images")
    fun getCorgiList(): Call<CorgiResponseDto>
}