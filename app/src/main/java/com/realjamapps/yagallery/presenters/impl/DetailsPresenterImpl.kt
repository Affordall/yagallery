package com.realjamapps.yagallery.presenters.impl

import com.realjamapps.yagallery.di.ActivityScope
import com.realjamapps.yagallery.presenters.IDetailsPresenter
import com.realjamapps.yagallery.views.IDetailsView
import java.lang.ref.WeakReference
import javax.inject.Inject

@ActivityScope
class DetailsPresenterImpl @Inject constructor(

) : IDetailsPresenter {

    private val TAG: String = "DetailsPresenter"

    private lateinit var viewReference: WeakReference<IDetailsView>

    override fun onAttach(view: IDetailsView) {
        viewReference = WeakReference(view)
    }

    override fun onResumeView() {

    }

    override fun onPauseView() {
    }

    override fun onDetach() {
        viewReference?.clear()
    }


}
