package com.realjamapps.yagallery.presenters.impl


import android.util.Log
import com.realjamapps.yagallery.di.ActivityScope
import com.realjamapps.yagallery.interactors.CorgiUseCase
import com.realjamapps.yagallery.logger.LogUtils
import com.realjamapps.yagallery.presenters.IMainPresenter
import com.realjamapps.yagallery.utils.AndroidDisposable
import com.realjamapps.yagallery.views.IMainView
import com.realjamapps.yagallery.views.custom.StateLayout
import io.reactivex.ObservableSource
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.SingleSubject
import java.lang.ref.WeakReference
import javax.inject.Inject

@ActivityScope
class MainPresenterImpl @Inject constructor(
        private val corgiUseCase: CorgiUseCase
) : IMainPresenter {

    private val TAG: String = "MainPresenter"

    private val disposable = AndroidDisposable()

    private lateinit var viewReference: WeakReference<IMainView>

    private var forceUpdate: Boolean = false

    override fun onAttach(view: IMainView) {
        viewReference = WeakReference(view)
    }

    override fun onForceUpdate() {
        forceUpdate = true
        loadCorgiList()
    }

    override fun onResumeView() {
        loadCorgiList()
    }

    private fun loadCorgiList() {
        Log.d(TAG, "onStartLoad")
        viewReference.get()?.setViewState(StateLayout.State.PROGRESS)

        val loadAndSaveFinishSubject = SingleSubject.create<Boolean>()

        if (forceUpdate) {
            Log.d(TAG, "fetch actual info")
            corgiUseCase.downloadListAndSaveToStorage()
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .doAfterTerminate {
                        loadAndSaveFinishSubject.onSuccess(true)
                    }
                    .subscribe(
                            {},
                            { error ->
                                LogUtils.logAndReportError(TAG, error)
                                viewReference.get()?.setViewState(StateLayout.State.ERROR)
                            }
                    )
                    .also { disposable.add(it) }
        }
        loadListFromStorage(if (forceUpdate) loadAndSaveFinishSubject.toObservable() else null)
    }

    private fun loadListFromStorage(delaySub: ObservableSource<Boolean>?) {
        Log.d(TAG, "fromDatabase")
        val loadSingle = corgiUseCase.loadListFromStorage()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
        delaySub?.let {
            loadSingle.delaySubscription(it)
        }
        loadSingle.subscribe(
                { list ->
                    val view = viewReference.get()
                    view?.updateAdapterList(list)
                    view?.setViewState(if (list.isNotEmpty()) StateLayout.State.CONTENT else StateLayout.State.EMPTY)
                },
                { error ->
                    LogUtils.logAndReportError(TAG, error)
                    viewReference.get()?.setViewState(StateLayout.State.ERROR)
                }
        )
                .also { disposable.add(it) }
    }

    override fun onPauseView() {
        disposable.dispose()
    }

    override fun onDetach() {
        viewReference?.clear()
    }


}
