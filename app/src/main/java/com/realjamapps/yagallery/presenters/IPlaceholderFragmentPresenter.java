package com.realjamapps.yagallery.presenters;


import com.realjamapps.yagallery.views.IMainView;

public interface IPlaceholderFragmentPresenter extends BaseFragmentPresenter<IMainView> {
    void onResume();
    void onPause();
    void onStart();
    void onStop();
    void onDestroy();
    void getDataOnStart();
    void updateData();
//    void onItemClick(Item item);
}
