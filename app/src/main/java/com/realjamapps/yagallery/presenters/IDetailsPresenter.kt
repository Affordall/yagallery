package com.realjamapps.yagallery.presenters

import com.realjamapps.yagallery.base.Presenter
import com.realjamapps.yagallery.views.IDetailsView

interface IDetailsPresenter : Presenter {
    fun onAttach(view: IDetailsView)
    fun onResumeView()
    fun onPauseView()
    fun onDetach()
}
