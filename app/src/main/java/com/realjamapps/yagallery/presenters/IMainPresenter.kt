package com.realjamapps.yagallery.presenters

import com.realjamapps.yagallery.base.Presenter
import com.realjamapps.yagallery.views.IMainView

interface IMainPresenter : Presenter {
    fun onAttach(view: IMainView)
    fun onForceUpdate()
    fun onResumeView()
    fun onPauseView()
    fun onDetach()
}
