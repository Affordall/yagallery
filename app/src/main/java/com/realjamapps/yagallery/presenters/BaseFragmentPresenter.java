package com.realjamapps.yagallery.presenters;

public interface BaseFragmentPresenter<T> {
    void init(T view);
}
