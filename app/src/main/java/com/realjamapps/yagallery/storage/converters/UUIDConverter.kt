package com.realjamapps.yagallery.storage.converters

import android.arch.persistence.room.TypeConverter
import java.util.*

internal object UUIDConverter {

    @TypeConverter
    @JvmStatic
    fun toUuid(uuid: String?): UUID? {
        return uuid?.let { UUID.fromString(it) }
    }

    @TypeConverter
    @JvmStatic
    fun toString(uuid: UUID?): String? {
        return uuid?.toString()
    }
}