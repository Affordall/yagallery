package com.realjamapps.yagallery.storage

import com.realjamapps.StorageTransactionManager
import com.realjamapps.yagallery.dao.CorgiDataDao
import com.realjamapps.yagallery.data.CorgiData
import java.util.*
import java.util.concurrent.locks.ReadWriteLock
import javax.inject.Inject
import javax.inject.Singleton
import kotlin.concurrent.withLock


@Singleton
class CorgiDataStorage @Inject internal constructor(
        private val databaseLock: ReadWriteLock,
        private val corgiDataDao: CorgiDataDao,
        private val storageTransactionManager: StorageTransactionManager
) {

    fun insert(corgiData: CorgiData) {
        storageTransactionManager.runGlobalOperationWithWriteLock {
            corgiDataDao.insert(corgiData)
        }
    }

    fun insertAll(corgiList: List<CorgiData>) {
        storageTransactionManager.runGlobalOperationWithWriteLock {
            corgiList.forEach { corgiData ->
                corgiDataDao.insert(corgiData)
            }
        }
    }

    fun findById(uuid: UUID): CorgiData? {
        return databaseLock.readLock().withLock {
            corgiDataDao.findById(uuid)
        }
    }

    fun loadAll(): List<CorgiData>? {
        return databaseLock.readLock().withLock {
            corgiDataDao.loadAll()
        }
    }
}