package com.realjamapps.yagallery

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import com.realjamapps.yagallery.dao.CorgiDataDao
import com.realjamapps.yagallery.data.CorgiData

@Database(version = 1,
        entities = arrayOf(
                CorgiData::class
        ))
abstract class Database : RoomDatabase() {
    internal abstract fun corgiDataDao(): CorgiDataDao
}