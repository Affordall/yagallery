package com.realjamapps.yagallery.utils

import android.content.Context
import android.net.ConnectivityManager

object InternetUtils {

    fun isConnected(context: Context): Boolean = (context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager).activeNetworkInfo?.isConnected
            ?: false
}
