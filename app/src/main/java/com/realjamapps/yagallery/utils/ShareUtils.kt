package com.realjamapps.yagallery.utils

import android.content.Context
import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.net.Uri
import android.os.Build
import android.os.Environment
import android.support.v4.content.FileProvider
import android.widget.ImageView
import java.io.File
import java.io.FileOutputStream
import java.io.IOException

object ShareUtils {

    fun getLocalBitmapUri(imageView: ImageView, context: Context): Uri? {
        val drawable = imageView.drawable
        val bmp: Bitmap?
        if (drawable is BitmapDrawable) {
            bmp = (imageView.drawable as BitmapDrawable).bitmap
        } else {
            return null
        }
        var bmpUri: Uri? = null
        try {
            val file = File(context.getExternalFilesDir(Environment.DIRECTORY_PICTURES), "share_image_" + System.currentTimeMillis() + ".png")
            FileOutputStream(file).use { stream ->
                bmp?.compress(Bitmap.CompressFormat.PNG, 90, stream)
            }
            bmpUri = if (Build.VERSION.SDK_INT >= 24) {
                FileProvider.getUriForFile(context, "realjamapps.yagallery.fileprovider", file)
            } else {
                Uri.fromFile(file)
            }
        } catch (e: IOException) {
            e.printStackTrace()
        }

        return bmpUri
    }
}