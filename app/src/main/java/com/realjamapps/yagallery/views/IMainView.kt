package com.realjamapps.yagallery.views

import com.realjamapps.yagallery.base.View
import com.realjamapps.yagallery.models.Corgi
import com.realjamapps.yagallery.views.custom.StateLayout

interface IMainView : View {
    fun updateAdapterList(updatedList: List<Corgi>)
    fun setViewState(state: StateLayout.State)
}
