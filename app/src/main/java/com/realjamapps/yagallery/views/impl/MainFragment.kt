package com.realjamapps.yagallery.views.impl

import android.app.Activity
import android.app.ActivityOptions
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.support.v7.widget.GridLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.realjamapps.yagallery.R
import com.realjamapps.yagallery.adapters.ItemAdapter
import com.realjamapps.yagallery.base.BaseFragment
import com.realjamapps.yagallery.models.Corgi
import com.realjamapps.yagallery.presenters.impl.MainPresenterImpl
import com.realjamapps.yagallery.utils.InternetUtils
import com.realjamapps.yagallery.views.IMainView
import com.realjamapps.yagallery.views.custom.StateLayout
import kotlinx.android.synthetic.main.fragment_main.*
import javax.inject.Inject


class MainFragment : BaseFragment(), IMainView {

    companion object {
        private const val REQUEST_CODE_VIEW_DETAILS = 100001
    }

    private val adapter = ItemAdapter()
    private val DEFAULT_GRID_SIZE = 1
    private var gridSize: Int = DEFAULT_GRID_SIZE

    @Inject
    lateinit var presenter: MainPresenterImpl

    init {
        adapter.onClickObservable
                .subscribe({ corgi ->
                    context?.let { context ->
                        val intent = Intent(context, DetailActivity::class.java)
                        intent.putExtra("Image", corgi)
                        context.startActivityWithIntent(intent)
                    }
                })
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        retainInstance = true
        this.getComponent(com.realjamapps.yagallery.di.components.MainActivityComponent::class.java).inject(this)
    }

    override fun getLayoutResource(): Int {
        return R.layout.fragment_main
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val result = super.onCreateView(inflater, container, savedInstanceState)
        gridSize = context?.resources?.getInteger(R.integer.large_grid_size) ?: DEFAULT_GRID_SIZE
        return result
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        this.presenter.onAttach(this)

        viewRecycler.layoutManager = GridLayoutManager(context, gridSize)
        viewRecycler.setHasFixedSize(true)
        viewRecycler.adapter = adapter

        viewSwipeRefreshLayout.setColorSchemeResources(android.R.color.holo_green_dark,
                android.R.color.holo_red_dark,
                android.R.color.holo_blue_dark,
                android.R.color.holo_orange_dark)

        viewSwipeRefreshLayout.setOnRefreshListener {
            presenter.onForceUpdate()
        }

        viewButtonLoad.setOnClickListener {
            presenter.onForceUpdate()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            when (requestCode) {
                REQUEST_CODE_VIEW_DETAILS -> data?.let {

                }
            }
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        if (InternetUtils.isConnected(context)) {
            presenter.onForceUpdate()
        } else {
            presenter.onResumeView()
        }
    }

    override fun onPause() {
        presenter.onPauseView()
        super.onPause()
    }

    override fun onDestroyView() {
        presenter.onDetach()
        viewRecycler?.adapter = null
        super.onDestroyView()
    }

    override fun updateAdapterList(updatedList: List<Corgi>) {
        adapter.updateData(updatedList)
        viewSwipeRefreshLayout.isRefreshing = false
    }

    override fun setViewState(state: StateLayout.State) {
        (stateLayout as StateLayout).state = state
    }

    private fun Context.startActivityWithIntent(intent: Intent) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            val options = ActivityOptions.makeSceneTransitionAnimation(activity)
            startActivity(intent, options.toBundle())
        } else {
            startActivity(intent)
        }
    }
}
