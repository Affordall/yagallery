package com.realjamapps.yagallery.views.custom;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;

import com.realjamapps.yagallery.R;


public class StateLayout extends ViewGroup {

    private State state = State.CONTENT;

    public StateLayout(Context context) {
        super(context);
    }

    public StateLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public StateLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        if (this.state != state) {
            this.state = state;
            for (int i = 0; i < getChildCount(); i++) {
                updateChildVisibility(getChildAt(i));
            }
        }
    }

    @Override
    public void addView(View child, int index) {
        super.addView(child, index);
        updateChildVisibility(child);
    }

    @Override
    public void addView(View child, int index, ViewGroup.LayoutParams params) {
        super.addView(child, index, params);
        updateChildVisibility(child);
    }

    @Override
    public void addView(View child, ViewGroup.LayoutParams params) {
        super.addView(child, params);
        updateChildVisibility(child);
    }

    @Override
    public void addView(View child, int width, int height) {
        super.addView(child, width, height);
        updateChildVisibility(child);
    }

    @Override
    public void addView(View child) {
        super.addView(child);
        updateChildVisibility(child);
    }

    private void updateChildVisibility(View child) {
        LayoutParams lp = (LayoutParams) child.getLayoutParams();
        child.setVisibility(lp.state == state ? View.VISIBLE : View.INVISIBLE);
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        int count = getChildCount();

        for (int childIndex = 0; childIndex < count; childIndex++) {
            View child = getChildAt(childIndex);
            if (child.getVisibility() != GONE) {
                child.layout(0, 0, child.getMeasuredWidth(), child.getMeasuredHeight());
            }
        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int count = getChildCount();

        int maxHeight = 0;
        int maxWidth = 0;

        // Find out how big everyone wants to be
        measureChildren(widthMeasureSpec, heightMeasureSpec);

        // Find rightmost and bottom-most child
        for (int childIndex = 0; childIndex < count; childIndex++) {
            View child = getChildAt(childIndex);
            if (child.getVisibility() != GONE) {
                maxWidth = Math.max(maxWidth, child.getMeasuredWidth());
                maxHeight = Math.max(maxHeight, child.getMeasuredHeight());
            }
        }

        // Check against minimum height and width
        maxHeight = Math.max(maxHeight, getSuggestedMinimumHeight());
        maxWidth = Math.max(maxWidth, getSuggestedMinimumWidth());

        setMeasuredDimension(resolveSize(maxWidth, widthMeasureSpec), resolveSize(maxHeight, heightMeasureSpec));
    }

    @Override
    protected ViewGroup.LayoutParams generateDefaultLayoutParams() {
        return new LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT, State.CONTENT);
    }

    @Override
    protected ViewGroup.LayoutParams generateLayoutParams(ViewGroup.LayoutParams lp) {
        return new LayoutParams(lp);
    }

    @Override
    public ViewGroup.LayoutParams generateLayoutParams(AttributeSet attrs) {
        return new LayoutParams(getContext(), attrs);
    }

    @Override
    protected boolean checkLayoutParams(ViewGroup.LayoutParams p) {
        return p instanceof LayoutParams;
    }

    public enum State {CONTENT, PROGRESS, ERROR, EMPTY}

    public static class LayoutParams extends ViewGroup.LayoutParams {

        private State state = State.CONTENT;

        public LayoutParams(Context c, AttributeSet attrs) {
            super(c, attrs);
            TypedArray a = c.obtainStyledAttributes(attrs, R.styleable.StateLayout_Layout);
            int state = a.getInt(R.styleable.StateLayout_Layout_viewForState, 3);
            a.recycle();

            switch (state) {
                case 0:
                    this.state = State.EMPTY;
                    break;
                case 1:
                    this.state = State.PROGRESS;
                    break;
                case 2:
                    this.state = State.ERROR;
                    break;
                case 3:
                    this.state = State.CONTENT;
                    break;
            }

        }

        public LayoutParams(int width, int height, State state) {
            super(width, height);
            this.state = state;
        }

        public LayoutParams(ViewGroup.LayoutParams source) {
            super(source);
        }

        public State getState() {
            return state;
        }

        public void setState(State state) {
            this.state = state;
        }
    }
}
