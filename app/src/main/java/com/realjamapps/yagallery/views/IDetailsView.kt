package com.realjamapps.yagallery.views

import com.realjamapps.yagallery.base.View
import com.realjamapps.yagallery.models.Corgi
import com.realjamapps.yagallery.views.custom.StateLayout

interface IDetailsView : View {
    fun setImage(urlImage: String)
}
