package com.realjamapps.yagallery.views.impl

import android.content.Intent
import android.os.Bundle
import android.support.annotation.LayoutRes
import android.support.design.widget.Snackbar
import android.support.design.widget.Snackbar.LENGTH_LONG
import android.view.Menu
import android.view.MenuItem
import com.bumptech.glide.Glide
import com.realjamapps.yagallery.R
import com.realjamapps.yagallery.base.BaseActivity
import com.realjamapps.yagallery.di.IHasComponent
import com.realjamapps.yagallery.di.components.DaggerDetailsActivityComponent
import com.realjamapps.yagallery.di.components.DetailsActivityComponent
import com.realjamapps.yagallery.models.Corgi
import com.realjamapps.yagallery.presenters.impl.DetailsPresenterImpl
import com.realjamapps.yagallery.utils.ShareUtils.getLocalBitmapUri
import com.realjamapps.yagallery.views.IDetailsView
import kotlinx.android.synthetic.main.activity_detail.*
import javax.inject.Inject


class DetailActivity : BaseActivity(),
        IHasComponent<DetailsActivityComponent>, IDetailsView {

    private lateinit var detailsActivityComponent: DetailsActivityComponent

    @Inject
    lateinit var presenter: DetailsPresenterImpl

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initializeInjector()
        setContentView(R.layout.activity_detail)

        this.getComponent().inject(this)

        intent.extras?.let { bundle ->
            val corgi: Corgi = bundle.getParcelable("Image")
            setImage(corgi.imageUrl)
        }
    }



    private fun initializeInjector() {
        this.detailsActivityComponent = DaggerDetailsActivityComponent.builder()
                .applicationComponent(applicationComponent)
                .activityModule(activityModule)
                .build()
    }

    override fun setContentView(@LayoutRes layoutResID: Int) {
        super.setContentView(layoutResID)
        toolbar?.let {
            setupToolbar()
        }
    }

    private fun setupToolbar() {
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp)
    }

    override fun setImage(urlImage: String) {
        Glide.with(this)
                .load(urlImage)
                .into(viewFullSizeImage)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // handle arrow click here
        when (item.itemId) {
            android.R.id.home -> onBackPressed()
            R.id.action_share -> onShareItem()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun getComponent(): DetailsActivityComponent {
        return detailsActivityComponent
    }

    override fun onStart() {
        super.onStart()
        presenter.onAttach(this)
    }

    override fun onResume() {
        super.onResume()
        presenter.onResumeView()
    }

    override fun onPause() {
        presenter.onPauseView()
        super.onPause()
    }

    override fun onDestroy() {
        presenter.onDetach()
        super.onDestroy()
    }

    private fun onShareItem() {
        val bmpUri = getLocalBitmapUri(viewFullSizeImage, this)
        if (bmpUri != null) {
            val shareIntent = Intent()
            shareIntent.action = Intent.ACTION_SEND
            shareIntent.putExtra(Intent.EXTRA_STREAM, bmpUri)
            shareIntent.type = "image/*"
            shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
            startActivity(Intent.createChooser(shareIntent, "Share Image"))
        } else {
            Snackbar.make(viewMainContent, "Sorry, can't share this image", LENGTH_LONG).show()
        }
    }
}
