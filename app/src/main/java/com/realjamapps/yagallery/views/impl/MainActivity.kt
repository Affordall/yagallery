package com.realjamapps.yagallery.views.impl

import android.os.Bundle
import android.support.annotation.LayoutRes
import android.view.Menu
import android.view.MenuItem
import com.realjamapps.yagallery.R
import com.realjamapps.yagallery.base.BaseActivity
import com.realjamapps.yagallery.di.IHasComponent
import com.realjamapps.yagallery.di.components.DaggerMainActivityComponent
import com.realjamapps.yagallery.di.components.MainActivityComponent
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : BaseActivity(),
        IHasComponent<MainActivityComponent> {

    private lateinit var mainActivityComponent: MainActivityComponent

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initializeInjector()
        setContentView(R.layout.activity_main)
    }

    private fun initializeInjector() {
        this.mainActivityComponent = DaggerMainActivityComponent.builder()
                .applicationComponent(applicationComponent)
                .activityModule(activityModule)
                .build()
    }

    override fun setContentView(@LayoutRes layoutResID: Int) {
        super.setContentView(layoutResID)
        toolbar?.let {
            setSupportActionBar(it)
        }
    }

    override fun getComponent(): MainActivityComponent {
        return mainActivityComponent
    }
}
