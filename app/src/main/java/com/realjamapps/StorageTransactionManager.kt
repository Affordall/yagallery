package com.realjamapps

import com.realjamapps.yagallery.Database
//import android.arch.persistence.room.Database
import java.util.concurrent.Callable
import java.util.concurrent.locks.ReadWriteLock
import java.util.concurrent.locks.ReentrantReadWriteLock
import javax.inject.Inject
import javax.inject.Singleton
import kotlin.concurrent.withLock


@Singleton
class StorageTransactionManager @Inject constructor(
        private val database: Database,
        private val databaseLock: ReadWriteLock
) {

    fun <T> runGlobalOperationWithWriteLock(operation: () -> T): T {
        databaseLock.writeLock().withLock {
            return database.withTransaction(operation)
        }
    }

    private fun <T> Database.withTransaction(operation: () -> T): T {
        return runInTransaction(Callable<T> { operation.invoke() })
    }
}